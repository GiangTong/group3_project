﻿using FA.Project.Model;
using System.Threading.Tasks;

namespace FA.Core.Services
{
    public interface IRequestServices : IBaseService<Request>
    {
        Task<bool> ExistAsync(int id);
    }
}
