﻿using FA.Project.Model;
using System.Threading.Tasks;

namespace FA.Core.Services
{
    public interface ITraineeServices : IBaseService<Trainee>
    {
        Task<bool> ExistAsync(int id);
    }
}
