﻿using FA.Core.Model;
using FA.Project.Model;
using System.Threading.Tasks;

namespace FA.Core.Repository
{
    public interface ITraineeRepository : IGenericRepository<Trainee>
    {
        Task<bool> ExistAsync(int id);
    }
}
