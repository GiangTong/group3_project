﻿using FA.Core.Model;
using FA.Project.Model;
using System.Threading.Tasks;

namespace FA.Core.Repository
{
    public interface IClassAdminRepository : IGenericRepository<ClassAdmin>
    {
        Task<bool> ExistAsync(int id);
    }
}
