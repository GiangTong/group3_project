﻿using FA.Core.Model;
using FA.Project.Model;
using System.Threading.Tasks;

namespace FA.Core.Repository
{
    public interface ITrainerRepository : IGenericRepository<Trainer>
    {
        Task<bool> ExistAsync(int id);
    }
}
