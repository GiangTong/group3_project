﻿using FA.Core.Model;
using FA.Project.Model;
using System.Threading.Tasks;


namespace FA.Core.Repository
{
    public interface IRequestRepository : IGenericRepository<Request>
    {
        Task<bool> ExistAsync(int id);
    }
}
