﻿using FA.Core.Model;
using FA.Project.Model;
using System.Threading.Tasks;

namespace FA.Core.Repository
{
    public interface IClassBatchRepository : IGenericRepository<ClassBatch>
    {
        Task<bool> ExistAsync(int id);
    }
}
