﻿using FA.Core.Services;
using FA.Project.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Fa.Present.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class Requests3Controller : Controller
    {
        private readonly TMSContext _context;
        private readonly IRequestServices _requestServices;
        public Requests3Controller(TMSContext context, IRequestServices requestServices)
        {
            _context = context;
            _requestServices = requestServices;
        }
        public async Task<IActionResult> Index()
        {
            var tMSContext = _context.Requests.Include(r => r.Trainee).Include(x => x.ClassAdmin);
            return View(await tMSContext.ToListAsync());
        }

        // GET: Admin/Requests3/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var request = await _requestServices.GetByIdAsync((int)id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        // GET: Admin/Requests3/Create
        public IActionResult Create()
        {
            ViewData["TraineeId"] = new SelectList(_context.Trainees, "TraineeId", "Account");
            ViewData["ClassAdminId"] = new SelectList(_context.ClassAdmins, "ClassAdminId", "Account");
            return View();
        }

        // POST: Admin/Requests3/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RequestId,Requesttype,Reason,StartDate,EndDate,ComimmingTime,LeavingTime,ExpectedApproval,ApprovedTime,Status,AuditTrail,isDelete,TraineeId,ApproverId")] Request request)
        {
            if (ModelState.IsValid)
            {
                _context.Add(request);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TraineeId"] = new SelectList(_context.Trainees, "TraineeId", "Account", request.TraineeId);
            ViewData["ClassAdminId"] = new SelectList(_context.ClassAdmins, "ClassAdminId", "Account", request.ApproverId);
            return View(request);
        }

        // GET: Admin/Requests3/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var request = await _context.Requests.FindAsync(id);
            // var request = await _context.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            ViewData["TraineeId"] = new SelectList(_context.Trainees, "TraineeId", "Account", request.TraineeId);
            ViewData["ClassAdminId"] = new SelectList(_context.ClassAdmins, "ClassAdminId", "Account", request.ApproverId);
            return View(request);
        }

        // POST: Admin/Requests3/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RequestId,Requesttype,Reason,StartDate,EndDate,ComimmingTime,LeavingTime,ExpectedApproval,ApprovedTime,Status,AuditTrail,isDelete,TraineeId,ApproverId")] Request request)
        {
            if (id != request.RequestId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _requestServices.UpdateAsync(request);
                    //_context.Update(request);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RequestExists(request.RequestId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TraineeId"] = new SelectList(_context.Trainees, "TraineeId", "Account", request.TraineeId);
            ViewData["ClassAdminId"] = new SelectList(_context.ClassAdmins, "ClassAdminId", "Account", request.ApproverId);
            return View(request);
        }

        // GET: Admin/Requests3/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var request = await _context.Requests
                .Include(r => r.Trainee)
                .FirstOrDefaultAsync(m => m.RequestId == id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        // POST: Admin/Requests3/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var request = await _context.Requests.FindAsync(id);
            _context.Requests.Remove(request);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RequestExists(int id)
        {
            return _context.Requests.Any(e => e.RequestId == id);
        }
    }
}
